import time
import pytest

import allure
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from faker import Faker

# Initialize Faker
fake = Faker()

# Create custom markers for Allure report categories (optional)
def pytest_html_report_title(report):
    report.title = "Automation Report"
@pytest.fixture(scope="session")
def pytest_allure_adaptor_exporter_args(parser):
    parser.addoption("--allure-severity", action="store", default=None, help="Set Allure severity label")

# Apply Allure categories to tests using custom markers (optional)
def pytest_collection_modifyitems(config, items):
    for item in items:
        if item.get_closest_marker("severity") is not None:
            severity = item.get_closest_marker("severity").args[0]
            item.add_marker(pytest.mark.allure_label(severity=severity))

@pytest.mark.parametrize("iteration", range(2))  # Set the number of iterations here
def test_purchase(iteration):
    # Create an Allure test case
    with (allure.step(f"Iteration {iteration + 2}")):
        serv_obj = Service("/Users/dit46/Browserdrivers/chromedriver-mac-arm64/chromedriver")
        driver = webdriver.Chrome(service=serv_obj)

        # Attach a screenshot to the Allure report (optional)
        # allure.attach(driver.get_screenshot_as_png(), name="screenshot", attachment_type=allure.attachment_type.PNG)


        try:
            driver.get("https://qa-store.mybigcommerce.com/")
            driver.maximize_window()
            driver.implicitly_wait(10)

            # Click search box
            search = driver.find_element(By.XPATH, "//span[normalize-space()='Search']").click()
            search_input = driver.find_element(By.ID, "search_query").send_keys("hem")

            # Click search product
            click_product = driver.find_element(By.XPATH,"//div[@id='quickSearch']//a[normalize-space()='Hem Egyptian Musk']").click()

            # Product require details select
            color = driver.find_element(By.XPATH, "//span[@title='Black']").click()

            # Select size
            size = driver.find_element(By.ID, "attribute_select_201")
            size_select = Select(size)

            # Select an option by visible text
            size_select.select_by_visible_text("Small")

            # Quantity increase
            quantity = driver.find_element(By.ID, "qty[]").clear()
            qty = driver.find_element(By.ID, "qty[]").send_keys(2)
            add_to_cart = driver.find_element(By.XPATH, "//input[@id='form-action-addToCart']").click()

            # Redirect to cart page
            cart_button_click = driver.find_element(By.XPATH, "//a[normalize-space()='View or edit your cart']").click()

            # Move to checkout page
            click_checkout = driver.find_element(By.XPATH, "//a[normalize-space()='Check out']").click()

            # Checkout page fill required details
            email = driver.find_element(By.ID, "email").send_keys(fake.email())
            submit_email = driver.find_element(By.XPATH, "//button[@id='checkout-customer-continue']").click()
            time.sleep(3)

            first_name = driver.find_element(By.ID, "firstNameInput").send_keys(fake.first_name())
            last_name = driver.find_element(By.ID, "lastNameInput").send_keys(fake.last_name())
            company = driver.find_element(By.ID, "companyInput").send_keys(fake.company())
            phone_number = driver.find_element(By.ID, "phoneInput").send_keys(fake.phone_number())
            address1 = driver.find_element(By.ID, "addressLine1Input").send_keys(fake.street_address())
            address2 = driver.find_element(By.ID, "addressLine2Input").send_keys(fake.secondary_address())
            city = driver.find_element(By.ID, "cityInput").send_keys("Ahmedabad")

            # Country dropdown select
            country = driver.find_element(By.ID, "countryCodeInput")
            country_select = Select(country)
            country_select.select_by_visible_text("India")


            # State select
            sate = driver.find_element(By.ID, "provinceCodeInput")
            sate_select = Select(sate)
            sate_select.select_by_visible_text("Gujarat")

            # Postcode
            postcode = driver.find_element(By.ID, "postCodeInput").send_keys(380015)

            # Scrolling
            driver.execute_script("window.scrollBy(0, 1000);")

            time.sleep(5)

            # Shipping radio button select
            # shipping_radio=driver.find_element(By.XPATH,"//div[@class='shippingOption shippingOption--alt']").click()

            # Continue button press
            wait = WebDriverWait(driver, 15)
            continue_button = wait.until(EC.element_to_be_clickable((By.XPATH, "//button[@id='checkout-shipping-continue']")))

            # Click the "Continue" button
            continue_button.click()

            # Payment option
            Bankdeposit=driver.find_element(By.XPATH,"//div[@data-test='payment-method-bankdeposit']").click()

            # payment = driver.find_element(By.XPATH, "//div[contains(text(),'Test Payment Provider')]").click()
            # # Locate and interact with the payment iframe
            # payment_iframe = driver.find_element(By.ID, "bigpaypay-ccName")
            # driver.switch_to.frame(payment_iframe)

            # Locate and interact with the card number field
            # card_number = driver.find_element(By.ID, "card-name")
            # card_number.send_keys("4111111111111111")
            # driver.switch_to.default_content()

            # iframecardnumber=driver.find_element(By.XPATH,"//div[@id='bigpaypay-ccNumber']//iframe")
            # driver.switch_to.frame(iframecardnumber)
            # iframecardnumber.send_keys('4111 1111 1111 1111')
            # Expiration=driver.find_element(By.ID,"bigpaypay-ccExpiry")
            # Expiration.send_keys("12/25")
            # driver.switch_to.default_content()

            payment_continue = driver.find_element(By.XPATH, "//button[@id='checkout-payment-continue']").click()

            time.sleep(8)
            allure.attach(driver.get_screenshot_as_png(), name="screenshot", attachment_type=allure.attachment_type.PNG)

        finally:
            # Close the browser
            driver.quit()

#allure run commond=pytest python_automation/PracticeTask_DIT/test_userflow_orderplace_withreport.py --alluredir="./reports"